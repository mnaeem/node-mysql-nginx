

module.exports = {
    parseCookies(req) {
        const parser = new Promise((resolve, reject) => {
            try {
                let userData = [];
                req.headers.cookie.split(';').forEach((headerString) => {
                    let cookie = headerString.split('=');
                    if (cookie[0].trim() === 'user') {
                        userData = cookie[1].split('-');
                    }
                });
                resolve(userData);
            } catch (e) {
                reject('cookie fail');
            }
        });

        return parser;
    }
};
