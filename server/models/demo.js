const Sequelize = require('sequelize');
const orm = require('../db/connect');

const Demo = orm.define('demos', {
    title: {
        type: Sequelize.STRING(150), // VARCHAR(150)
        allowNull: false,
    },
    description: {
        type:  Sequelize.TEXT,
        defaultValue: '',
    },
});

Demo.associate = function(models) {
    // associations can be defined here
};

module.exports = {
    model: Demo,
    createDemoData() {
        return new Promise((resolve) => {
            Demo.findAndCountAll()
                .then((result) => {
                    if (result.count === 0) {
                        Demo.create({
                            title:'First one',
                            description: 'Created by sync',
                        }).then(() => {
                            resolve();
                        });
                    } else {
                        resolve();
                    }
                });
        });
    }
};
