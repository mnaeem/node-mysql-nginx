const Sequelize = require('sequelize');
const Op = Sequelize.Op;
const orm = require('../db/connect');

const Session = orm.define('session', {
    userId: {
        type: Sequelize.INTEGER,
        allowNull: false,
    },
    userHash: {
        type: Sequelize.STRING,
        allowNull: false
    },
    expires: {
        type: Sequelize.BIGINT,
        allowNull: false,
    },
});

Session.associate = function(models) {
    // associations can be defined here
};

function deleteOld() {
    return Session.destroy({
        where: {
            expires: {
                [Op.lt]: (Date.now()),
            }
        }
    })
}

/**
 *
 * @param {integer} userId
 * @param {integer} expires
 * @param {string} userHash
 */
function addSession(userId, userHash, expires) {
    deleteSessionByUser(userId)
        .then(() => {
            Session.create({
                userId,
                userHash,
                expires,
            });
        });
}

/***
 *  A user can only have one valid session
 *
 * @param {integer} userId
 * @returns {Promise<undefined>}
 */
function deleteSessionByUser(userId) {
    return Session.destroy({
        where: {
            userID: userId,
        }
    });
}

module.exports = {
    model: Session,
    /**
     *
     * @param {integer} userId
     * @param {string} userHash
     * @param {integer} expires
     * @returns {void|PromiseLike<T | never>|Promise<T | never>|*}
     */
    addSession(userId, userHash, expires) {
        return deleteOld()
            .then(() => {
                addSession(userId, userHash, expires);
            })
    },

    updateExpire (result) {
        return Session.update({
            expires: Date.now() + 60 * 60 * 1000,
        }, {
            where: {
                id: result.id,
            }
        });
    },

    sessionValid(userHash, userId) {
        return Session.findOne({
            where: {
                userHash,
                userId,
                expires: {
                    [Op.gte]: Date.now(),
                }
            }
        }).then((result) => {
            if (result) {
                return this.updateExpire(result);
            }
            throw new Error('Session expired');
        })

    }

};
