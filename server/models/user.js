const Sequelize = require('sequelize');
const orm = require('../db/connect');
const bcrypt = require('bcrypt');

const User = orm.define('users', {
    username: {
        type: Sequelize.STRING,
        unique: false,
        allowNull: false
    },
    email: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false
    },
    lastlog: {
        type: Sequelize.STRING,
    }
}, {
    hooks: {
        /**
         * encrypt the password, so that we have no plain password in the db
         * @param {Sequelize} user
         */
        beforeCreate: (user) => {
            user.password = bcrypt.hashSync(
                user.password,
                bcrypt.genSaltSync()
            );
        }
    },
});

User.associate = function(models) {
    // associations can be defined here
};

/**
 *
 * @param {string} password
 * @param {Sequelize} user
 * @returns {*}
 */
function validPassword(password, user) {
    return bcrypt.compareSync(password, user.dataValues.password);
}

function createDemoUser() {
    console.log('create demo user');
    User.create({
        username: 'DemoUser',
        email: 'demo@demo.de',
        password: 'verySecure',
    });
}

module.exports = {
    model: User,
    /**
     *
     * @returns {Promise<any>}
     */
    login(req) {
        let username = req.body.username;
        let password = req.body.password;

        return new Promise((resolve, reject) => {
            User.findOne({where: {username: username}})
                // checks if the user is in result
                .then((user) => {
                    if (!user) {
                        resolve({
                            valid: false,
                            error: 1,
                        });
                    }
                    return user;
                })
                // validates password
                .then((user) => {
                    if (!validPassword(password, user)) {
                        resolve({
                            valid: false,
                            error: 2,
                        });
                    }
                    return user;
                })
                // credentials valid
                .then((user) => {
                    resolve({
                        valid: true,
                        id: user.id,
                    })
                });
        })
    }
};
