const express = require('express');
const cors = require('cors');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const chalk = require('chalk');
const app = express();
// disable express header
app.disable('x-powered-by');
require('../rest/client')(app);
// enable morgen request logger with dev format
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cors());
app.set('trust proxy', 1); // trust first proxy
app.set('port', 8081);

require('../routes/router')(app);
app.listen(app.get('port'), () => {
    console.log(chalk.green('Server started on localhost:' + app.get('port'))
        , chalk.red('\nPress Ctrl-C to terminate.'));
});
