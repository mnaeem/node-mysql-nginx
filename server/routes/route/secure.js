module.exports = function (app, middleware) {

    app.route('/secure').get(middleware, (req, res) => {
        res.send(JSON.stringify({
            content: 'secure content',
        }))
    })
}
