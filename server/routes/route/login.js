const auth = require('../../auth/auth');

module.exports = (app) => {

    // route for user login
    app.route('/login')
        .get((req, res) => {
            res.send('<h1>Login</h1>');
        })
        .post(auth);
};

