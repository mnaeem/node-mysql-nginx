module.exports = (app) => {
    const sessionChecker = require('../session/session')(app);
    // index route
    app.get('/', require('./route/index'));

    // login
    require('./route/login')(app);

    // secure
    require('./route/secure')(app, sessionChecker);


}
