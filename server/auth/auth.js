const User = require('../models/user');
const uniqid = require('uniqid');
const sessionModel = require('../models/session');

module.exports = function (req, res) {
    res.clearCookie('user');
    res.clearCookie('user_sid');
    User.login(req)
        .then((state) => {
            if (state.valid === false) {
                throw new Error('invalid login');
            } else {
                return state;
            }
        })
        .then((state) => {
            req.session.user = state.id;
            let userHash = uniqid();
            let timestamp = Date.now() + 60 * 60 * 1000;

            res.cookie('user', `${userHash}-${state.id}`, {
                    expires: new Date(timestamp),
                    httpOnly: true
                }
            );

            sessionModel.addSession(state.id, userHash, timestamp)
                .then(() => {
                    res.send(JSON.stringify({success: true}))
                });

        }).catch((err) => {
                console.log(err);
                res.sendStatus(401);
        });
};
