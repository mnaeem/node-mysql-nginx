const cookieParser = require('cookie-parser');
const session = require('express-session');

module.exports = function(app) {
    // initialize cookie-parser to allow access the cookies stored in the browser.
    app.use(cookieParser());
    app.use(session({
            key: 'user_sid',
            secret: 'some_secret', // is required
            resave: false,
            saveUninitialized: false,
            cookie: {
                expires: new Date(Date.now() + 60 * 60 * 1000), // 1 hour
            }
        })
    );

    // This middleware will check if user's cookie is still saved in browser and user is not set, then automatically log the user out.
    // This usually happens when you stop your express server after login, your cookie still remains saved in the browser.
    app.use((req, res, next) => {
        if (req.cookies.user_sid && !req.session.user) {
            res.clearCookie('user_sid');
        }
        next();
    });

    // middleware function to check for logged-in users
    return (req, res, next) => {
        if (req.session.user && req.cookies.user_sid) {
            next();
        } else {
            res.send(401); // send no authorization header
        }
    };
};
