const epilogue = require('epilogue');
const db = require('../db/connect');
module.exports = function (app) {
    // enable epilogue rest client
    epilogue.initialize({
        app: app,
        sequelize: db
    });
    require('./enpoint/demoData');
};
