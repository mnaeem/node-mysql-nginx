const epilogue = require('epilogue');
const DemoData = require('../../models/demo');
const sessionChecker = require('../secure');

epilogue.resource({
    model: DemoData.model,
    endpoints: ['/demoData', '/demoData/:id'],
});

const secure = epilogue.resource({
    model: DemoData.model,
    endpoints: ['/demoSecure', '/demoSecure/:id']
});

secure.list.fetch.before(sessionChecker);
