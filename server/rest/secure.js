const Session = require('../models/session');
const cookieUtils = require('../utils/cookie');
const chalk = require('chalk');

module.exports = function (req, res, context) {

    function reject(err) {
        console.log(chalk.red(err));
        context.stop();
        res.sendStatus(401); // send no authorization header
    }

    cookieUtils.parseCookies(req)
        .then((userData) => {
            Session.sessionValid(...userData).then(() => {
                res.cookie('user', userData.join('-'), {
                    expires: new Date(Date.now() + 60 * 60 * 1000),
                });
                context.continue();
            }).catch(reject);
        }).catch(reject)
};
